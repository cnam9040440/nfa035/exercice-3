package fr.cnam.foad.nfa035.fileutils.streaming.test;

import fr.cnam.foad.nfa035.fileutils.streaming.media.ImageByteArrayFrame;
import fr.cnam.foad.nfa035.fileutils.streaming.serializer.ImageDeserializerBase64StreamingImpl;
import fr.cnam.foad.nfa035.fileutils.streaming.serializer.ImageSerializerBase64StreamingImpl;
import fr.cnam.foad.nfa035.fileutils.streaming.serializer.ImageStreamingDeserializer;
import fr.cnam.foad.nfa035.fileutils.streaming.serializer.ImageStreamingSerializer;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class StreamingTestJunit {

    private static final Logger LOGGER =  LogManager.getLogger( StreamingTest.class );

    @Test
    /**
     * Test junit permettant de tester la sérialisation et la désérialisation
     */
    void serializeDeserialize() throws IOException {
        File image = new File("petite_image_2.png");
        ImageByteArrayFrame media = new ImageByteArrayFrame(new ByteArrayOutputStream());

        // Sérialisation
        ImageStreamingSerializer serializer = new ImageSerializerBase64StreamingImpl();
        serializer.serialize(image, media);

        String encodedImage = media.getEncodedImageOutput().toString();
        LOGGER.log(Level.INFO, encodedImage + "\n");

        ByteArrayOutputStream deserializationOutput = new ByteArrayOutputStream();
        ImageStreamingDeserializer deserializer = new ImageDeserializerBase64StreamingImpl(deserializationOutput);

        deserializer.deserialize(media);
        byte[] deserializedImage = ((ByteArrayOutputStream)deserializer.getSourceOutputStream()).toByteArray();
        // Vérification
        // 1/ Automatique
        byte[] originImage = Files.readAllBytes(image.toPath());
        assert Arrays.equals(originImage, deserializedImage);
        LOGGER.log(Level.INFO, "Cette sérialisation est bien réversible :)");

        //  2/ Manuelle
        File extractedImage = new File("petite_image_extraite.png");
        new FileOutputStream(extractedImage).write(deserializedImage);
        LOGGER.log(Level.INFO, "Je peux vérifier moi-même en ouvrant mon navigateur de fichiers et en ouvrant l'image extraite dans le répertoire de ce Test");
    }

}